import 'dart:convert';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:toast/toast.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import 'package:page_transition/page_transition.dart';
import 'package:rialotestproject/SignIn.dart';
import 'package:rialotestproject/models/auth.dart';
import 'package:rialotestproject/sharedPrefs/authPrefs.dart';
import 'package:rialotestproject/url/urls.dart';
import 'dart:io' show  Platform;
import 'dart:async';
import '../main_page.dart';
class AuthService
{
  void signIn( AnimationController controller,  BuildContext context  ,var  _scafKey,var email , var password ,  ) async{
    try{
     // controller.forward();
      Map<String, String> header = {
        'Content-Type': 'application/json-patch+json' ,
        'Accept': 'text/plain',
      };
      var url = WebServiceUrl.auth + '?email=$email&password=$password' ;
      print(url);
      var response  = await  http.get(url , headers: header) ;
      controller.reverse() ;
      int statusCode = response.statusCode ;
      print('status code = '+statusCode.toString());
      if(statusCode == 200)
      {
        Auth authModel =  Auth.fromJson(jsonDecode(response.body))  ;
        print(authModel.data.accessToken) ;
        AuthPrefs authPrefs = AuthPrefs(token:authModel.data.accessToken, authState: true) ;
        await authPrefs.storeAuthState() ;
        var token = await authPrefs.getToken() ;
        var authState = await authPrefs.checkAuth() ;
        print(token);
        print(authState);

        if(Platform.isAndroid)
        {
          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, curve: Curves.easeOut, child: MainPage()));
        }else
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()));
        }
        Timer(Duration(milliseconds: 100) , (){
          Toast.show("Wellcome", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM , backgroundColor: CustomColors.accentColor );

        });
      }else{
        Timer(Duration(milliseconds: 100) , (){
        Toast.show("Somethings went wrong!", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM , backgroundColor: CustomColors.accentColor );

        });

      }
    }catch(e)
    {
      Timer(Duration(milliseconds: 100) , (){
        Toast.show("Somethings went wrong!", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM , backgroundColor: CustomColors.accentColor );
      });


    }
  }

}