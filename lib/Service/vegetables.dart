import 'dart:convert';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/models/vegetables.dart';
import 'package:toast/toast.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import 'package:page_transition/page_transition.dart';
import 'package:rialotestproject/SignIn.dart';
import 'package:rialotestproject/models/auth.dart';
import 'package:rialotestproject/sharedPrefs/authPrefs.dart';
import 'package:rialotestproject/url/urls.dart';
import 'dart:io' show  Platform;

import '../main_page.dart';
class VegetablesService
{
  Future<Vegetables> getVegetables(  ) async{
    Map<String, String> header = {
      'Content-Type': 'application/json-patch+json' ,
      'Accept': 'text/plain',
    };
    AuthPrefs authPrefs = AuthPrefs() ;
    var token = await authPrefs.getToken() ;

    var url = WebServiceUrl.vegetables + '?access_token=$token';
    print(url);
    var response  = await  http.get(url , headers: header) ;
    int statusCode = response.statusCode ;
    print('status code = '+statusCode.toString());
    //print(response.body);

    try{
      if(statusCode == 200)
      {
        return Vegetables.fromJson(jsonDecode(response.body)) ;
      }else{


        return  null ;
      }
    }catch(e)
    {
      print(e.toString());

      return null ;
    }
  }

}