import 'package:dynamic_custom_theme/theme/dynamic.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/models/authField.dart';
import 'package:rialotestproject/widgets/SignIn_form.dart';
import 'package:rialotestproject/widgets/signIn_btn.dart';
import 'Service/authService.dart';
import 'package:toast/toast.dart';
import 'Theme/global.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  AuthField authField = AuthField() ;
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var autoValidate = false ;
  var t = false ;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.primaryColor,
      body: SingleChildScrollView(
        child:Container(
          child: Column(
            children: <Widget>[
//              Container(
//                width: MediaQuery.of(context).size.width,
//                height: 250,
//                child: Center(
//                  child: CachedNetworkImage(
//                    imageUrl: "https://www.thoughtco.com/thmb/19F0cna2JSUcDnkuv7oUiSYALBQ=/768x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/lotus-flower-828457262-5c6334b646e0fb0001dcd75a.jpg",
//                    progressIndicatorBuilder: (context, url, downloadProgress) =>
//                        CircularProgressIndicator(value: downloadProgress.progress),
//                    errorWidget: (context, url, error) => Icon(Icons.error),
//                  ),
//                ),
//              ),
              Container(
                child:  Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top:  MediaQuery.of(context).size.height*0.15 ,  bottom: MediaQuery.of(context).size.height*0.1 ),
                        child: Center(
                          child: Text('Sign In' , style: TextStyle(
                              color: CustomColors.accentColor ,
                              fontSize: 19
                          ),),
                        )
                    ) ,
                    //SignInForm(this.authField) ,
                    Container(
                      child: Form(
                          key: _formKey,
                          child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 10 , bottom: 4 , left: 4 , right: 4),
                                  margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
                                  decoration: BoxDecoration(
                                      color: CustomColors.lightPrimaryColor ,
                                      borderRadius: BorderRadius.all(Radius.circular(15))
                                  ),
                                  child: TextFormField(
                                    style: TextStyle(color: CustomColors.lightText  , fontSize: 19),
                                    cursorColor: CustomColors.accentColor,
                                    decoration: InputDecoration(
                                      prefixIcon: Container(
                                        child: Center(
                                          child: Image.asset('assets/images/mail.png' , width: 28 , height: 28,),
                                        ) ,
                                        width: 40,
                                        padding: EdgeInsets.only(bottom: 5),
                                      ),
                                      border: InputBorder.none,
                                      hintText: 'Email' ,
                                      hintStyle: TextStyle(color: CustomColors.lightText , fontSize: 19),
                                      errorStyle: TextStyle(
                                        color: Colors.redAccent
                                      )
                                    ),
                                    autovalidate: autoValidate,
                                    onChanged: (txt){
                                      authField.setMail(txt);
                                    },
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                  ),
                                ) ,
                                SizedBox(
                                  height: 15,
                                ),
                                Container(
                                  padding: EdgeInsets.all(4),
                                  margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
                                  decoration: BoxDecoration(
                                      color: CustomColors.lightPrimaryColor ,
                                      borderRadius: BorderRadius.all(Radius.circular(15))
                                  ),
                                  child: TextFormField(
                                    obscureText: true,
                                    style: TextStyle(color: CustomColors.lightText  , fontSize: 19),
                                    cursorColor: CustomColors.accentColor,
                                    decoration: InputDecoration(
                                      prefixIcon: Container(
                                        child: Center(
                                          child: Image.asset('assets/images/podlock.png' , width: 30 , height: 30,),
                                        ) ,
                                        width: 50,
                                        padding: EdgeInsets.only(bottom: 5),
                                      ),
                                      border: InputBorder.none,
                                      hintText: 'Password' ,
                                      hintStyle: TextStyle(color: CustomColors.lightText , fontSize: 19),
                                        errorStyle: TextStyle(
                                            color: Colors.redAccent
                                        )
                                    ),
                                    onChanged: (txt){
                                      authField.setPass(txt);
                                    },
                                    autovalidate: autoValidate,
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      return null;
                                    },
                                  ),
                                ) ,
                              ]
                          )
                      ),
                    ) ,
                    Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 35 ,  bottom: MediaQuery.of(context).size.height*0.1 ),
                        child: Center(
                          child: Text('Forgot Password? ' , style: TextStyle(
                              color: CustomColors.lightText ,
                              fontSize: 17
                          ),),
                        )
                    ) ,
                    SizedBox(height: 30,) ,
                  //  SignInBtn(this.authField),
                    Container(
                      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.1 ),
                      width: MediaQuery.of(context).size.width ,
                      height: 55,
                      padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
                      child: ProgressButton(
                        color: CustomColors.accentColor,
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        strokeWidth: 2,
                        child:Container(
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image.asset('assets/images/right.png' , width: 20 , height: 25,) ,
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "SIGN IN",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      height: 1.5
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        onPressed: (AnimationController controller) async{
                          controller.forward() ;
                          setState(() {
                            autoValidate =  true ;
                          });
                          if (_formKey.currentState.validate()) {
                            AuthService authService = AuthService() ;

                            //print(authField.getMail() +'  /  ' + authField.getPass()) ;
                            await authService.signIn(controller, context , _scaffoldKey , authField.getMail(), authField.getPass());
                          }

                          setState(() {
                            controller.reverse();
                          });
                        },
                      ),
                    ) ,
                    SizedBox(height: 30,) ,
                    FlatButton(
                      child: Text('testing' , style: TextStyle(
                          color: snapshot.data.text
                      ),),
                      onPressed: ()async {
                        CustomTheme<DynamicTheme> custom = CustomTheme();

                        print(t.toString()) ;
                        if(t) {
                          await custom.setThemes(lightMode);
                          themeSwitched = false;
                        }
                        else {
                          await custom.setThemes(darkMode);
                          themeSwitched = true;
                        }
                        setState(() {
                          if(t)
                            t=false ;
                          else
                          t=true ;
                        });
                        //Navigator.push(context, MaterialPageRoute(builder:(_)=>Anim() )) ;
                      },
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(30) , bottomLeft: Radius.circular(30)),
                  color: CustomColors.darkBackColor,
                ),
                margin: EdgeInsets.only(bottom: 20),
              ) ,
              SizedBox(
                height: 5,
              ),
              Container(
                height: 70,
                width: MediaQuery.of(context).size.width,
                color: CustomColors.primaryColor,
                child: Center(
                  child: Container(
                      child: Center(
                        child: Text('CREATE ACCOUNT ' , style: TextStyle(
                            color: CustomColors.lightText,
                            fontSize: 16
                        ),),
                      )
                  ) ,
                ),
              ),
            ],
          ),
        ),
        ),
    );
  }
}
