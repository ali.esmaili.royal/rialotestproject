import 'dart:async';

import 'package:flutter/material.dart';
import 'dart:math' as math;
class Anim extends StatefulWidget {
  @override
  _AnimState createState() => _AnimState();
}

class _AnimState extends State<Anim> with TickerProviderStateMixin{
  AnimationController animationController ;
  AnimationController lineAnimController ;
  Animation<double> lineAnim ;
  Animation<double> animation ;
  @override
  void initState() {
    animationController = AnimationController(
      vsync: this ,
      duration: Duration(seconds: 3)
    ) ;
    lineAnimController = AnimationController(
        vsync: this ,
      duration: Duration(seconds: 1)
    ) ;

    final curvedAnimation = CurvedAnimation(parent: animationController, curve: Curves.bounceIn ,  reverseCurve: Curves.easeIn);
   final lineAnimation = CurvedAnimation(parent: lineAnimController, curve: Curves.easeInQuart , reverseCurve: Curves.easeInSine) ;

    animation = Tween<double> (begin: 0 ,  end: 2*math.pi)
        .animate(curvedAnimation)
        ..addListener((){
          setState(() {});
        })
    ..addStatusListener((status){
      if(status==AnimationStatus.completed)
        {
          Timer(Duration(milliseconds: 500), (){
            animationController.reverse();
          });
        }else if (status==AnimationStatus.dismissed){
        animationController.forward();
      }
    });
    lineAnim = Tween<double>(begin: 0 ,  end: 150).animate(lineAnimation)
    ..addListener((){
      setState(() {

      });
    })
    ..addStatusListener((status){
      if(status==AnimationStatus.completed)
        {
          lineAnimController.reverse() ;
        }else if(status==AnimationStatus.dismissed)
          {
            lineAnimController.forward() ;
          }
    });
    animationController.forward();
    lineAnimController.forward();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size ;
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 50),
              width: mediaQuery.width ,
              height: 160,
              child: Transform.rotate(
                angle: animation.value,
                child: Container(
                  alignment: Alignment.center,
                  child: FlutterLogo(
                    size: 150,
                  ),
                ),
              ),
            ) ,
            Divider(
              height: 50,
            ),
            Container(
              width: mediaQuery.width ,
              height: 160,
              padding:EdgeInsets.only(left:lineAnim.value),
              alignment: Alignment.centerLeft,
              child: FlutterLogo(
                size: 150,
              ),
            )
          ],
        ),
      )
    );
  }
  @override
  void dispose() {
    animationController.dispose() ;
    lineAnimController.dispose();
    super.dispose();
  }
}
