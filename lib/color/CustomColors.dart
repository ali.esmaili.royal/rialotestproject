import 'dart:ui';

mixin CustomColors implements Color {
  static  Color primaryColor = hexToColor('#404E5A');
  static  Color darkPrimaryColor = hexToColor('#5D4037');
  static  Color darkBackColor = hexToColor('#45535E');
  static  Color lightPrimaryColor = hexToColor('#4E5D6A');
  static  Color accentColor = hexToColor('#7BED8D');
  static  Color primaryText = hexToColor('#212121');
  static  Color secondaryText  = hexToColor('#757575');
  static  Color lightText  = hexToColor('#A6BCD0');
  static  Color appBarText  = hexToColor('#748A9D');
  static  Color dividerColor  = hexToColor('#BDBDBD');
  static  Color fabColor  = hexToColor('#748A9D');
  static  Color redColor  = hexToColor('#DD4040');
  static  Color orangeColor  = hexToColor('#FFA700');
  static  Color yellowColor  = hexToColor('#FFD958');
  static  Color lightGreenColor  = hexToColor('#D2F475');
  static  Color editBackColor  = hexToColor('#748A9D');
  static  Color deleteBackColor  = hexToColor('#A6BCD0');

   static Color hexToColor(String code) {
     return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
   }
}
