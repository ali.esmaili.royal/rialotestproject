import 'dart:async';
import 'package:dynamic_custom_theme/theme/dynamic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rialotestproject/SignIn.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/main_page.dart';
import 'package:rialotestproject/models/authField.dart';

import 'Theme/global.dart';
import 'sharedPrefs/authPrefs.dart';

void main(){
  runApp(
//    MultiProvider(
////      providers: [
////        ChangeNotifierProvider(create: (context) => AuthField()),
////      ],
//      child:
//    ),
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context ) {

    return StreamBuilder<DynamicTheme>(

      /// [custom.streamColors] This is your stream in your package.
      /// You need to initialize this one.
        stream: custom.streamColors.stream,

        /// of course your need to pass your Initial so that your
        /// app will not throw an error.
        /// [initial] came from globals.
        initialData: initial,

        /// [Snapshot] this will be your data when you pass your Themes.
        builder: (context, snap) {

          /// Make sure [DON'T FORGET THIS ONE]
          /// this is your snapshot that you can call anypage of your projects.
          /// this came from global so you can easily call your custom theme in any pages.
          /// before return your widgets.
          snapshot = snap;
          return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
                primaryColor: CustomColors.primaryColor,
                accentColor: CustomColors.accentColor,
                fontFamily: 'Acumin' ,
                brightness: snap.data.brightness
            ), //   home: Splash(),
            debugShowCheckedModeBanner: false,
            initialRoute: '/',
            routes: <String, WidgetBuilder>{
              '/': ( context) =>  Splash(),
              '/SignIn': ( context) =>  SignIn(),
              '/mainPage': ( context) =>  MainPage(),
            },
          );
        }
    );
  }
}

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  var leftOne = 1000.0 ;
  var rightOne = 1000.0 ;
  var topOne = 1000.0 ;
  var bottomOne = 1000.0 ;

  var leftTwo = 1000.0 ;
  var rightTwo = 1000.0 ;
  var topTwo = 1000.0 ;
  var bottomTwo = 1000.0 ;

  var leftThree = 1000.0 ;
  var rightThree = 1000.0 ;
  var topThree = 1000.0 ;
  var bottomThree = 1000.0 ;

  var loading = true ;

  initAnim(){
    Timer(Duration(milliseconds: 100), () {
      setState(() {
        leftOne = -1000.0 ;
        rightOne = -1000.0 ;
        topOne = -1000.0 ;
        bottomOne = -1000.0 ;
      });
      Timer(Duration(milliseconds: 150), () {
        setState(() {
          leftTwo = -1000.0 ;
          rightTwo = -1000.0 ;
          topTwo = -1000.0 ;
          bottomTwo= -1000.0 ;
        });
        Timer(Duration(milliseconds: 200), () {
          setState(() {
            leftThree = -1000.0 ;
            rightThree = -1000.0 ;
            topThree = -1000.0 ;
            bottomThree = -1000.0 ;
          });
        });
      });
    });


  }
  @override
  void initState() {
    checkAuth() ;
    initAnim() ;
    super.initState();
  }
  checkAuth() async {
    AuthPrefs authPrefs = AuthPrefs() ;
    var token = await authPrefs.getToken() ;
    var authState = await authPrefs.checkAuth() ;
    print(token);
    print(authState);
    var _duration = new Duration(milliseconds:2000);
    if(authState)
      {
        Timer(_duration, (){
          Navigator.of(context).pushReplacementNamed('/mainPage');
        });
      }else
        {
          Timer(_duration, (){
            Navigator.of(context).pushReplacementNamed('/SignIn');
          });
        }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.accentColor,
      body: Stack(
        children: <Widget>[
          AnimatedPositioned(
            duration: Duration(milliseconds: 2000),
            left: leftOne,
            right: rightOne ,
            top: topOne,
            bottom: bottomOne ,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: CustomColors.editBackColor,
              ),
            ),
          ) ,
          AnimatedPositioned(
            duration: Duration(milliseconds: 2000),
            left: leftTwo,
            right: rightTwo,
            top: topTwo,
            bottom: bottomTwo ,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: CustomColors.lightText,
              ),
            ),
          ) ,
          AnimatedPositioned(
            duration: Duration(milliseconds: 2000),
            left: leftThree,
            right: rightThree,
            top: topThree,
            bottom: bottomThree ,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: CustomColors.primaryColor,
              ),
            ),
          ) ,

          Center(
              child: Container(
                width: 120,
                height: 120,
                child: Image.asset('assets/images/logo.png' , fit: BoxFit.fill,),
              )
          ) ,
        ],
      ),
    );
  }
}
