import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/widgets/cart_tab.dart';
import 'package:rialotestproject/widgets/favorite_tab.dart';
import 'package:rialotestproject/widgets/quickshop_tab.dart';
import 'package:rialotestproject/widgets/settings_tab.dart';
import 'package:rialotestproject/widgets/store_tab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import './bloc/cart_count.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with SingleTickerProviderStateMixin {
  static CounterBloc counterBloc = new CounterBloc(initialCount: 0);
  final List<Widget> _tabs=[
    StoreTab(counterBloc: counterBloc,),
    FavoriteTab(),
    QuickShopTab(),
    CartTab(counterBloc: counterBloc,),
    SettingsTab(),
  ];
  int selectedPos ;
  int _tabIndex = 0 ;
  void _onItemTapped(int index) {
    if(index==2)
      {

      }else
        {
          _animationController.reverse();
          setState(() {
            _tabIndex = index;
            closeIcon = false;

          });
          isOpened = false;
        }
  }
  Future<bool> _onBackPressed(){
    print(_tabIndex) ;
    if(_tabIndex == 0)
    {
      showDemoDialog(
        context: context,
        child:CupertinoTheme(
          data: CupertinoThemeData(
              brightness: Brightness.light
          ),
          child:  CupertinoAlertDialog(
            title: const Text('Do you want to Exit ? ' , style: TextStyle(
                color: Colors.black87,
                fontFamily: 'Acumin',
                height: 1.9
            ),),
            actions: <Widget>[
              CupertinoDialogAction(
                child: const Text('Yes' , style: TextStyle(
                  color: Colors.redAccent,
                  fontFamily: 'Acumin',
                ),),
                isDestructiveAction: true,
                onPressed: ()=>SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
              ),
              CupertinoDialogAction(
                child: const Text('No' , style: TextStyle(
                  color: Colors.black87,
                  fontFamily: 'Acumin',
                ),),
                isDefaultAction: true,
                onPressed: () => Navigator.pop(context,'no'),
              ),
            ],
          ),
        ),
      );
    }else{
      setState(() {
        _tabIndex = 0 ;
      });
    }
  }
  void showDemoDialog({BuildContext context, Widget child}) {
    showCupertinoDialog<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        //setState(() { lastSelectedValue = value; });
      }
    });
  }

  GlobalKey _bottomNavigationKey = GlobalKey();
  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _animateColor;
  Animation<double> _animateIcon;
  Curve _curve = Curves.easeOut;
  @override
  initState() {

    _animationController =
    AnimationController(vsync: this, duration: Duration(milliseconds: 500))
      ..addListener(() {
        setState(() {});
      });
    _animateIcon = Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _animateColor = ColorTween(
      begin: CustomColors.lightText,
      end:CustomColors.accentColor,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        0.80,
        curve: _curve,
      ),
    ));
    super.initState();
  }
  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }
  animate() {
    if (!isOpened) {
      setState(() {
        _tabIndex = 2;
        closeIcon = !closeIcon;
      });
      _animationController.forward();
    } else {
      _animationController.reverse();
      setState(() {
        _tabIndex = 0;
        closeIcon = !closeIcon;
      });
    }
    isOpened = !isOpened;
    setState(() {
    });
  }
  bool closeIcon = false ;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
          margin: EdgeInsets.only(top: 60),
          child: FloatingActionButton(
            backgroundColor: _animateColor.value,
            onPressed: animate,
            child:closeIcon!=true?
            ImageIcon(new AssetImage('assets/images/close.png') , color: Colors.white,)
                :
            ImageIcon(new AssetImage('assets/images/open.png'), color: Colors.white,) ,
          ),
        ),
        bottomNavigationBar: Visibility(
          child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0.0),
                  color: CustomColors.primaryColor
              ),
              padding: EdgeInsets.only(left:15 , right:15 ,bottom: 20),
              child:new Theme(
                data: new ThemeData( splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,),
                child: BottomNavigationBar(
                  elevation: 0,
                  type: BottomNavigationBarType.fixed,
                  onTap: _onItemTapped,
                  unselectedItemColor: CustomColors.fabColor,
                  selectedItemColor:  CustomColors.accentColor ,
                  currentIndex: _tabIndex,
                  key: _bottomNavigationKey ,
                  backgroundColor: CustomColors.primaryColor,
                  showSelectedLabels: false,
                  showUnselectedLabels: false,
                  items: <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                        icon: ImageIcon(new AssetImage('assets/images/home.png')),
                        title: Text('')
                    ),
                    BottomNavigationBarItem(
                        icon: ImageIcon(new AssetImage('assets/images/fav.png')),
                        title: Text('')
                    ),
                    BottomNavigationBarItem(
                      icon: Image.asset('assets/images/fav.png' , width: 30 , height: 30 ,),
                      title: Text(''),
                    ),
                    BottomNavigationBarItem(
                        icon:StreamBuilder<int>(
                              stream: counterBloc.counterObservable,
                              builder: (context , snapshot){
                                return Badge(
                            badgeContent:
                            Text(snapshot.data.toString() ,  style: TextStyle(
                                    color: Colors.white ,
                                    fontSize: 15
                                ),) , 
                            animationType: BadgeAnimationType.scale,
                            animationDuration: Duration(milliseconds: 200),
                            badgeColor: CustomColors.accentColor,
                            position: BadgePosition.topLeft(top: 6 , left: 18),
                            child: ImageIcon(new AssetImage('assets/images/cart.png'))
                               );
                              },
                            ),
                        title: Text('')
                    ),
                    BottomNavigationBarItem(
                        icon: ImageIcon(new AssetImage('assets/images/settings.png')),
                        title: Text('')
                    ),
                  ],
                ),
              )
          ),
          visible: true,
        ),
        body: _tabs[_tabIndex],
      ),
    );
  }
}
