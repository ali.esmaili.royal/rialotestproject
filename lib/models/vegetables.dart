import 'dart:convert';

class Vegetables {
  int status;
  List<Data> data;

  Vegetables({this.status, this.data});

  Vegetables.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();

      json['data'].forEach((value) {
        var _value = value ;
        if(_value['status'].toString()=='1')
        data.add(new Data.fromJson(value));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String name;
  String image;
  String description;
  int status;

  Data({this.name, this.image, this.description, this.status});

  Data.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    image = json['image'];
    description = json['description'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    data['description'] = this.description;
    data['status'] = this.status;
    return data;
  }
}