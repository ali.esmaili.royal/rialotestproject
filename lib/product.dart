import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rialotestproject/bloc/cart_count.dart';
import 'package:rialotestproject/widgets/add_cart_btn.dart';
import 'package:rialotestproject/widgets/product_desc.dart';
import 'package:rialotestproject/widgets/product_info.dart';
import 'package:rialotestproject/widgets/product_title.dart';
import 'color/CustomColors.dart';

class Product extends StatefulWidget {
  var img  , title , description ;
  CounterBloc counterBloc ;
  Product({this.img , this.title , this.description , this.counterBloc}) ;
  @override
  _ProductState createState() => _ProductState(this.img , this.title , this.description);
}

class _ProductState extends State<Product> {
  var img  , title , description ;
  _ProductState(this.img , this.title , this.description) ;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.primaryColor,
      appBar:  AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: CustomColors.primaryColor,
        leading: IconButton(icon: ImageIcon(new AssetImage('assets/images/back.png') , color: CustomColors.appBarText,), onPressed: (){
          Navigator.pop(context);
        }),
        title: Text(title , style: TextStyle(
            color: CustomColors.appBarText
        ),),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.1 , right: MediaQuery.of(context).size.width*0.1),
                  width: MediaQuery.of(context).size.width*0.65,
                  child:Container(
                    child:
                    Container(
                      width: MediaQuery.of(context).size.width *0.8,
                      child: Center(
                        child: CachedNetworkImage(
                          imageUrl: img,
                          progressIndicatorBuilder: (context, url, downloadProgress) =>
                              CupertinoActivityIndicator(),
                          //  errorWidget: (context, url, error) => Icon(Icons.error),
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                  )
              ) ,
              Container(
                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.1 , right: MediaQuery.of(context).size.width*0.1),
                width: MediaQuery.of(context).size.width,
                height: 65,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: CustomColors.darkBackColor,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 5),
                      child: Text(title,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.appBarText ,
                            fontSize: 17
                        ),),
                    ) ,
                    Container(
                      padding: EdgeInsets.only(top: 5),
                      child: Text('2 heads',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.lightText ,
                            fontSize: 17
                        ),),
                    ) ,
                    Container(
                      padding: EdgeInsets.only(top: 5),
                      child: Text('£0.80',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.appBarText ,
                            fontSize: 17
                        ),),
                    ) ,
                    Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Icon(Icons.expand_more ,  color:CustomColors.appBarText ,)
                    ) ,
                  ],
                ),
              ) ,
              AddToCartBtn(counterBloc : widget.counterBloc),
              ProductTitle(txt: 'Description',) ,
              ProductDescription(txt: description,) ,
              ProductTitle(txt: 'Storage',) ,
              ProductDescription(txt: 'For maximum freshness, keep refrigerated. Wash before use.',) ,
              ProductTitle(txt: 'Origin',) ,
              ProductDescription(txt: 'Produce of United Kingdom, Republic of Ireland, Germany, Italy, Netherlands, Poland, Spain, USA',) ,
              ProductTitle(txt: 'Preparation & Usage',) ,
              ProductDescription(txt: 'Wash before use. Trim as required.',) ,
              ProductTitle(txt: 'Nutritional Information',) ,
              ProductInfo(
                nutritionalInformation: 'Serving Size',
                amount: '250g',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Calories',
                amount: '477Kcal',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Protein',
                amount: '10g',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Carbohydrates',
                amount: '20g',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Calories',
                amount: '5g',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Calories',
                amount: '2g',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Sugar',
                amount: '5g',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Fibre',
                amount: '3g',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Saturated Fat',
                amount: '2mg',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Cholesterol ',
                amount: '2mg',
              ) ,
              ProductInfo(
                nutritionalInformation: 'Sodium',
                amount: '2mg',
              ) ,

            ],
          ),
        ),
      ),
    );
  }
}
