import 'package:shared_preferences/shared_preferences.dart';
class AuthPrefs {
  final token;
  final authState;

  AuthPrefs({this.token, this.authState});

  storeAuthState() async
  {
    var prefs;
    prefs = await SharedPreferences.getInstance();
    prefs.setBool("userAuthState"  , authState);
    prefs.setString("accessToken"  , token);
  }

  Future<bool> checkAuth() async
  {
    var prefs;
    prefs = await SharedPreferences.getInstance();
    var authState = prefs.getBool("userAuthState");
    print(authState);

    if (authState == null || authState == false) {
      return false;
    } else {
      return true;
    }
  }

  Future<String> getToken() async
  {
    var prefs;
    prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("accessToken");
    if (token == null || token == false) {
      return '';
    } else {
      return token;
    }
  }
}