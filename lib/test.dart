//import 'package:flutter/material.dart';
//
//import 'bloc/cart_count.dart';
//
//class Test extends StatefulWidget {
//  Test({Key key, this.title}) : super(key: key);
//
//  final String title;
//
//  @override
//  _Test createState() => new _Test();
//}
//
//class _Test extends State<Test> {
//
//  CounterBloc _counterBloc = new CounterBloc(initialCount: 0);
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      appBar: new AppBar(
//      ),
//      body: new Center(
//        child: new Column(
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            new Text('You have pushed the button this many times:'),
//            new StreamBuilder(stream: _counterBloc.counterObservable, builder: (context, AsyncSnapshot<int> snapshot){
//              return new Text('${snapshot.data}', style: Theme.of(context).textTheme.display1);
//            })
//          ],
//        ),
//      ),
//      floatingActionButton: new Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
//        new Padding(padding: EdgeInsets.only(bottom: 10), child:
//          new FloatingActionButton(
//            onPressed: _counterBloc.increment,
//            tooltip: 'Increment',
//            child: new Icon(Icons.add),
//          )
//        ),
//        new FloatingActionButton(
//          onPressed: _counterBloc.decrement,
//          tooltip: 'Decrement',
//          heroTag: 't',
//          child: new Icon(Icons.remove),
//        ),
//      ])
//    );
//  }
//
//  @override
//  void dispose() {
//    _counterBloc.dispose();
//    super.dispose();
//  }
//
//}
import 'package:flutter/material.dart';

class Test extends StatelessWidget {
  Test({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(12.0, 20.0),
            child: Container(
              width: 351.0,
              height: 69.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(21.0),
                color: const Color(0xffffce00),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(12.0, 115.0),
            child: Container(
              width: 351.0,
              height: 499.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(19.0),
                color: const Color(0xffff6767),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
