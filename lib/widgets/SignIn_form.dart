import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:progress_indicator_button/button_stagger_animation.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:rialotestproject/models/authField.dart';

class SignInForm extends StatefulWidget {
  AuthField authField ;
  SignInForm(this.authField);
  @override
  _SignInFormState createState() => _SignInFormState(this.authField);
}

class _SignInFormState extends State<SignInForm> {
  final _formKey = GlobalKey<FormState>();
  AuthField authField ;
  _SignInFormState(this.authField);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
          key: _formKey,
          child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 10 , bottom: 4 , left: 4 , right: 4),
                  margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
                  decoration: BoxDecoration(
                      color: CustomColors.lightPrimaryColor ,
                      borderRadius: BorderRadius.all(Radius.circular(15))
                  ),
                  child: TextFormField(
                    style: TextStyle(color: CustomColors.lightText  , fontSize: 19),
                    cursorColor: CustomColors.accentColor,
                    decoration: InputDecoration(
                      prefixIcon: Container(
                        child: Center(
                          child: Image.asset('assets/images/mail.png' , width: 28 , height: 28,),
                        ) , 
                        width: 40,
                        padding: EdgeInsets.only(bottom: 5),
                      ),
                      border: InputBorder.none,
                      hintText: 'Email' ,
                      hintStyle: TextStyle(color: CustomColors.lightText , fontSize: 19),
                    ),
                    onChanged: (txt){

                      authField.setPass(txt);
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                ) ,
                SizedBox(
                  height: 15,
                ),
                Container(
                  padding: EdgeInsets.all(4),
                  margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
                  decoration: BoxDecoration(
                      color: CustomColors.lightPrimaryColor ,
                      borderRadius: BorderRadius.all(Radius.circular(15))
                  ),
                  child: TextFormField(
                    obscureText: true,
                    style: TextStyle(color: CustomColors.lightText  , fontSize: 19),
                    cursorColor: CustomColors.accentColor,
                    decoration: InputDecoration(
                      prefixIcon: Container(
                        child: Center(
                          child: Image.asset('assets/images/podlock.png' , width: 30 , height: 30,),
                        ) , 
                        width: 50,
                        padding: EdgeInsets.only(bottom: 5),
                      ),
                      border: InputBorder.none,
                      hintText: 'Password' ,
                      hintStyle: TextStyle(color: CustomColors.lightText , fontSize: 19),
                    ),
                    onChanged: (txt){
                      authField.setPass(txt);
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                ) ,
              ]
          )
      ),
    );
  }
}
