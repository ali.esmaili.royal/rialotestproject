import 'package:flutter/material.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:rialotestproject/bloc/cart_count.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

class AddToCartBtn extends StatefulWidget {
  CounterBloc counterBloc ; 
  AddToCartBtn({this.counterBloc});
  @override
  _AddToCartBtnState createState() => _AddToCartBtnState();
}

class _AddToCartBtnState extends State<AddToCartBtn> {


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.05 ),
      width: MediaQuery.of(context).size.width ,
      height: 55,
      padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
      child: ProgressButton(
        color: CustomColors.accentColor,
        borderRadius: BorderRadius.all(Radius.circular(100)),
        strokeWidth: 2,
        child:Container(
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/images/addToCart.png' , width: 30 , height: 30,) ,
                SizedBox(
                  width: 5,
                ),
                Text(
                  "ADD TO CART",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      height: 1.5
                  ),
                )
              ],
            ),
          ),
        ),
        onPressed: (AnimationController controller) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.SCALE,
            title: 'Success',
            desc: 'Successfully added to shopping cart.',
            headerAnimationLoop: false ,

          )..show();
          widget.counterBloc.increment();
           //Navigator.push(context, MaterialPageRoute(builder: (_)=>Test()));
        },
      ),
    );
  }

}
