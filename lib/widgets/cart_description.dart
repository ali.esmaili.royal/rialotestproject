import 'package:flutter/material.dart';
import 'package:rialotestproject/color/CustomColors.dart';
class CartDesc extends StatefulWidget {
  @override
  _CartDescState createState() => _CartDescState();
}

class _CartDescState extends State<CartDesc> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 50 , left:   MediaQuery.of(context).size.width*0.1 , right:   MediaQuery.of(context).size.width*0.1  ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Sub-total ' , style: TextStyle(
                  color: CustomColors.appBarText ,
                  fontSize: 18
              ),),
              Text('£9.30' , style: TextStyle(
                  color: CustomColors.appBarText ,
                  fontSize: 18
              ),),
            ],
          ) ,
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Delivery' , style: TextStyle(
                  color: CustomColors.appBarText ,
                  fontSize: 18
              ),),
              Text('Standard (free)' , style: TextStyle(
                  color: CustomColors.appBarText ,
                  fontSize: 18
              ),),
            ],
          ) ,
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Total ' , style: TextStyle(
                  color: CustomColors.appBarText ,
                  fontSize: 23
              ),),
              Text('£9.30' , style: TextStyle(
                  color: CustomColors.appBarText ,
                  fontSize: 23
              ),),
            ],
          ) ,
        ],
      ),
    );
  }
}
