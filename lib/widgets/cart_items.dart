import 'dart:async';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rialotestproject/bloc/cart_count.dart';
import 'package:rialotestproject/color/CustomColors.dart';
class CartItems extends StatefulWidget {
  CounterBloc counterBloc ; 
  CartItems({this.counterBloc});
  @override
  _CartItemsState createState() => _CartItemsState();
}

class _CartItemsState extends State<CartItems> {
  var  costRight = 1.0;
  var costOpacity = 1.0 ;
  var costVisible = true ;
  var itemsWidth = 0.0 ;
  var costWidth = 0.0 ;
  var firstInit = true ;
  var itemCornerRadius = 15.0 ;
  SlidableController slidableController;
  @override
  void initState() {
//      costRight = 1.0;
//     costOpacity = 1.0 ;
//     costVisible = true ;
//     itemsWidth = 0.0 ;
//     costWidth = 0.0 ;
//     firstInit = true ;
//     itemCornerRadius = 15.0 ;
    slidableController = SlidableController(
      onSlideAnimationChanged: handleSlideAnimationChanged,
      onSlideIsOpenChanged: handleSlideIsOpenChanged,
    ) ;
    super.initState();
  }
  Animation<double> _rotationAnimation;

  void handleSlideAnimationChanged(Animation<double> slideAnimation) {
    setState(() {
      _rotationAnimation = slideAnimation;
    });
  }

  void handleSlideIsOpenChanged(bool isOpen) {
    if(isOpen)
    {
      setState(() {
        itemCornerRadius = 0.0;
      });
    }else
      {
        setState(() {
          itemCornerRadius = 15.0 ;
        });
      }
  }
  @override
  Widget build(BuildContext context) {
    if(firstInit)
    {
      costWidth = MediaQuery.of(context).size.width*0.35 ;
      firstInit = false ;
    }
    return Column(
      children: <Widget>[
        Container(
          height: 108,
          padding: EdgeInsets.only(top: 19 ),
          child: Center(
            child: Text('Cart' , style: TextStyle(
                color: CustomColors.appBarText ,
                fontSize: 20
            ),),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(30) , bottomLeft: Radius.circular(30)),
            color: CustomColors.darkBackColor,
          ),
          margin: EdgeInsets.only(bottom: 20),
        ) ,
        Container(
          padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.07 , vertical: 8),
          child: Column(
            children: <Widget>[
              Container(
                height: 65,
                padding: EdgeInsets.only(left: 5),
                margin: EdgeInsets.symmetric( vertical: 8),
                decoration: BoxDecoration(
                    color: CustomColors.lightPrimaryColor ,
                    borderRadius: BorderRadius.all(Radius.circular(15))
                ),
                child: Stack(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Image.asset('assets/images/broccoli.png' ,
                            width: 65 ,  height: 65,
                            fit: BoxFit.fill,
                          ),
                        ) ,
                        SizedBox(width: 10,),
                        Container(
                          height: 70,
                          width: MediaQuery.of(context).size.width*0.3,
                          padding: EdgeInsets.only(top: 25),
                          child: Text('Broccoli',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: CustomColors.appBarText ,
                                fontSize: 17
                            ),),
                        ) ,
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end ,
                      children: <Widget>[
                        Visibility(
                          visible: costVisible,
                          child: AnimatedContainer(
                            duration: Duration(milliseconds: 300),
                            width: costWidth,
                            // margin: EdgeInsets.only(right:costRight ),
                            child: GestureDetector(
                              child:AnimatedOpacity(
                                duration: Duration(milliseconds: 50),
                                opacity: costOpacity,
                                child:  Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                      height: 70,
                                      width: MediaQuery.of(context).size.width*0.18,
                                      padding: EdgeInsets.only(top: 24),
                                      child: Text('2Heads',
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: CustomColors.lightText ,
                                            fontSize: 17
                                        ),),
                                    ) ,
                                    Container(
                                      height: 70,
                                      width: MediaQuery.of(context).size.width*0.12,
                                      padding: EdgeInsets.only(top: 25),
                                      margin: EdgeInsets.only(right:18 ),
                                      child: Text('£3.00',
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: CustomColors.appBarText ,
                                            fontSize: 15
                                        ),),
                                    ) ,
                                  ],
                                ),
                              ),
                              onTap: (){
                                cartItemOptions() ;
                              },
                            ),
                          ),
                        ),
                        Visibility(
                          visible: true ,//!costVisible,
                          child: AnimatedContainer(
                            width: itemsWidth,
                            duration: Duration(milliseconds: 300),
                            curve: Curves.easeInSine,
                            child: GestureDetector(
                              child:Container(
                                child:  Row(
                                  children: <Widget>[
                                    Container(
                                      height: 70,
                                      width: MediaQuery.of(context).size.width*0.15,
                                      child: IconButton(
                                        icon: ImageIcon(new AssetImage('assets/images/edit.png') , color: Colors.white,),
                                        color: Colors.transparent,
                                        disabledColor: Colors.transparent,
                                        focusColor:  Colors.transparent,
                                        highlightColor:  Colors.transparent,
                                        hoverColor:  Colors.transparent,
                                        splashColor:  Colors.transparent,
                                        onPressed: (){
                                          cartItemOptions() ;
                                        } ,),
                                      decoration:BoxDecoration(
                                        color: CustomColors.editBackColor ,
                                      ),
                                    ) ,
                                    Container(
                                      height: 70,
                                      width: MediaQuery.of(context).size.width*0.15,
                                      child: IconButton(
                                          icon: ImageIcon(new AssetImage('assets/images/delete.png') ,
                                            color:Colors.white,),
                                          color: Colors.transparent,
                                          disabledColor: Colors.transparent,
                                          focusColor:  Colors.transparent,
                                          highlightColor:  Colors.transparent,
                                          hoverColor:  Colors.transparent,
                                          splashColor:  Colors.transparent,
                                          onPressed: (){
                                            widget.counterBloc.decrement() ;
                                            cartItemOptions() ;
                                          }),
                                      decoration:BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(15),
                                            bottomRight: Radius.circular(15)
                                        ),
                                        color: CustomColors.deleteBackColor ,
                                      ),
                                    ) ,
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ) ,
              Slidable(
                controller: slidableController,
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.35,
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 300),
                  height: 65,
                  padding: EdgeInsets.only(left: 5),
                  margin: EdgeInsets.symmetric(vertical: 8),
                  decoration: BoxDecoration(
                      color: CustomColors.lightPrimaryColor ,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(itemCornerRadius) ,
                        topRight: Radius.circular(itemCornerRadius) ,
                        bottomLeft: Radius.circular(itemCornerRadius) ,
                        bottomRight: Radius.circular(itemCornerRadius)
                      )
                  ),
                  child: Stack(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Image.asset('assets/images/broccoli.png' ,
                              width: 65 ,  height: 65,
                              fit: BoxFit.fill,
                            ),
                          ) ,
                          SizedBox(width: 10,),
                          Container(
                            height: 70,
                            width: MediaQuery.of(context).size.width*0.3,
                            padding: EdgeInsets.only(top: 25),
                            child: Text('Broccoli',
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: CustomColors.appBarText ,
                                  fontSize: 17
                              ),),
                          ) ,
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end ,
                        children: <Widget>[
                          Visibility(
                            visible: true,
                            child: AnimatedContainer(
                              duration: Duration(milliseconds: 300),
                              width: MediaQuery.of(context).size.width*0.35,
                              // margin: EdgeInsets.only(right:costRight ),
                              child: GestureDetector(
                                child:AnimatedOpacity(
                                  duration: Duration(milliseconds: 50),
                                  opacity: 1,
                                  child:  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        height: 70,
                                        width: MediaQuery.of(context).size.width*0.18,
                                        padding: EdgeInsets.only(top: 24),
                                        child: Text('2Heads',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              color: CustomColors.lightText ,
                                              fontSize: 17
                                          ),),
                                      ) ,
                                      Container(
                                        height: 70,
                                        width: MediaQuery.of(context).size.width*0.12,
                                        padding: EdgeInsets.only(top: 25),
                                        margin: EdgeInsets.only(right:18 ),
                                        child: Text('£3.00',
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              color: CustomColors.appBarText ,
                                              fontSize: 15
                                          ),),
                                      ) ,
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  Visibility(
                    visible: true ,//!costVisible,
                    child: AnimatedContainer(
                      width: itemsWidth,
                      duration: Duration(milliseconds: 300),
                      curve: Curves.easeInSine,
                      child: GestureDetector(
                        child:Container(
                          child:  Row(
                            children: <Widget>[
                              Container(
                                height: 65,
                                width: MediaQuery.of(context).size.width*0.15,
                                child: IconButton(
                                    icon: ImageIcon(new AssetImage('assets/images/delete.png') ,
                                      color:Colors.white,),
                                    color: Colors.transparent,
                                    disabledColor: Colors.transparent,
                                    focusColor:  Colors.transparent,
                                    highlightColor:  Colors.transparent,
                                    hoverColor:  Colors.transparent,
                                    splashColor:  Colors.transparent,
                                    onPressed: (){
                                      //   cartItemOptions() ;
                                    }),
                                decoration:BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      bottomLeft: Radius.circular(15)
                                  ),
                                  color: CustomColors.deleteBackColor ,
                                ),
                              ) ,
                              Container(
                                height: 65,
                                width: MediaQuery.of(context).size.width*0.15,
                                child: IconButton(
                                  icon: ImageIcon(new AssetImage('assets/images/edit.png') , color: Colors.white,),
                                  color: Colors.transparent,
                                  disabledColor: Colors.transparent,
                                  focusColor:  Colors.transparent,
                                  highlightColor:  Colors.transparent,
                                  hoverColor:  Colors.transparent,
                                  splashColor:  Colors.transparent,
                                  onPressed: (){
                                    //cartItemOptions() ;
                                  } ,),
                                decoration:BoxDecoration(
                                  color: CustomColors.editBackColor ,
                                ),
                              ) ,
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
                secondaryActions: <Widget>[
                  Visibility(
                    visible: true ,//!costVisible,
                    child: AnimatedContainer(
                      width: itemsWidth,
                      duration: Duration(milliseconds: 300),
                      curve: Curves.easeInSine,
                      child: GestureDetector(
                        child:Container(
                          child:  Row(
                            children: <Widget>[
                              Container(
                                height: 65,
                                width: MediaQuery.of(context).size.width*0.15,
                                child: IconButton(
                                  icon: ImageIcon(new AssetImage('assets/images/edit.png') , color: Colors.white,),
                                  color: Colors.transparent,
                                  disabledColor: Colors.transparent,
                                  focusColor:  Colors.transparent,
                                  highlightColor:  Colors.transparent,
                                  hoverColor:  Colors.transparent,
                                  splashColor:  Colors.transparent,
                                  onPressed: (){
                                    //cartItemOptions() ;
                                  } ,),
                                decoration:BoxDecoration(
                                  color: CustomColors.editBackColor ,
                                ),
                              ) ,
                              Container(
                                height: 65,
                                width: MediaQuery.of(context).size.width*0.15,
                                child: IconButton(
                                    icon: ImageIcon(new AssetImage('assets/images/delete.png') ,
                                      color:Colors.white,),
                                    color: Colors.transparent,
                                    disabledColor: Colors.transparent,
                                    focusColor:  Colors.transparent,
                                    highlightColor:  Colors.transparent,
                                    hoverColor:  Colors.transparent,
                                    splashColor:  Colors.transparent,
                                    onPressed: (){
                                      Slidable.of(context)?.close();
                                      //   cartItemOptions() ;
                                    }),
                                decoration:BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(15),
                                      bottomRight: Radius.circular(15)
                                  ),
                                  color: CustomColors.deleteBackColor ,
                                ),
                              ) ,
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],

              ),
              Container(
                height: 65,
                padding: EdgeInsets.all(4),
                margin: EdgeInsets.symmetric( vertical: 8),
                decoration: BoxDecoration(
                    color: CustomColors.lightPrimaryColor ,
                    borderRadius: BorderRadius.all(Radius.circular(15))
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Image.asset('assets/images/kale.png' ,
                        width: 65 ,  height: 65,
                        fit: BoxFit.fill,
                      ),
                    ) ,
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.38,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('Kale',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.appBarText ,
                            fontSize: 17
                        ),),
                    ) ,
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.12,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('300g',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.lightText ,
                            fontSize: 17
                        ),),
                    ) ,
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.12,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('£3.00',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.appBarText ,
                            fontSize: 15
                        ),),
                    ) ,
                  ],
                ),
              ) ,
              Container(
                height: 65,
                padding: EdgeInsets.all(4),
                margin: EdgeInsets.symmetric( vertical: 8),
                decoration: BoxDecoration(
                    color: CustomColors.lightPrimaryColor ,
                    borderRadius: BorderRadius.all(Radius.circular(15))
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Image.asset('assets/images/pepper.png' ,
                        width: 65 ,  height: 65,
                        fit: BoxFit.fill,
                      ),
                    ) ,
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.38,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('Red Pappers',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.appBarText ,
                            fontSize: 17
                        ),),
                    ) ,
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.12,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('5',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.lightText ,
                            fontSize: 17
                        ),),
                    ) ,
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.12,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('£3.00',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.appBarText ,
                            fontSize: 15
                        ),),
                    ) ,
                  ],
                ),
              ) ,
              Container(
                height: 65,
                padding: EdgeInsets.all(4),
                margin: EdgeInsets.symmetric( vertical: 8),
                decoration: BoxDecoration(
                    color: CustomColors.lightPrimaryColor ,
                    borderRadius: BorderRadius.all(Radius.circular(15))
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Image.asset('assets/images/strawberry.png' ,
                        width: 65 ,  height: 65,
                        fit: BoxFit.fill,
                      ),
                    ) ,
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.3,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('Strawberries',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.appBarText ,
                            fontSize: 17
                        ),),
                    ) ,
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.21,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('2 punnets',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.lightText ,
                            fontSize: 17
                        ),),
                    ) ,
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width*0.12,
                      padding: EdgeInsets.only(top: 20),
                      child: Text('£3.00',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: CustomColors.appBarText ,
                            fontSize: 15
                        ),),
                    ) ,
                  ],
                ),
              ) ,
            ],
          ),
        )
      ],
    );
  }
  void cartItemOptions(){
    setState(() {
      costRight = 1.0 ;
      if(itemsWidth ==0.0)
      {
        itemsWidth = MediaQuery.of(context).size.width*0.30 ;
        Timer(Duration(milliseconds: 80), (){
          setState(() {
            costOpacity = 0.0;
          });
        });
      }else
      {
        Timer(Duration(milliseconds: 80), (){
          setState(() {
            costOpacity = 1.0;
          });
        });
        itemsWidth = 0 ;
        costWidth = MediaQuery.of(context).size.width*0.35 ;
      }
    });

    Timer(Duration(milliseconds: 100), (){
      setState(() {
       // costOpacity = 1.0 ;
      });
      Timer(Duration(milliseconds: 50), (){
        setState(() {
          costVisible = !costVisible;
        });
      });
    });
  }
}
