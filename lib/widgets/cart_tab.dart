import 'package:flutter/material.dart';
import 'package:rialotestproject/bloc/cart_count.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/widgets/cart_description.dart';
import 'package:rialotestproject/widgets/cart_items.dart';
import 'package:rialotestproject/widgets/checkout_btn.dart';
class CartTab extends StatefulWidget {
  CounterBloc counterBloc  ; 
  CartTab({this.counterBloc});
  @override
  _CartTabState createState() => _CartTabState();
}

class _CartTabState extends State<CartTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.primaryColor,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            CartItems(counterBloc: widget.counterBloc,) ,
            CartDesc() ,
            CheckoutBtn()
          ],
        ),
      )
    );
  }
}
