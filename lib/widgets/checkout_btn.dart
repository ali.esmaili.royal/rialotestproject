import 'dart:async';
import 'package:flutter/material.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/main_page.dart';
class CheckoutBtn extends StatefulWidget {
  @override
  _CheckoutBtnState createState() => _CheckoutBtnState();
}

class _CheckoutBtnState extends State<CheckoutBtn> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20 ),
      width: MediaQuery.of(context).size.width ,
      height: 55,
      padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
      child: ProgressButton(
        color: CustomColors.accentColor,
        borderRadius: BorderRadius.all(Radius.circular(100)),
        strokeWidth: 2,
        child:Container(
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/images/right.png' , width: 20 , height: 25,) ,
                SizedBox(
                  width: 5,
                ),
                Text(
                  "CHECKOUT",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      height: 1.5
                  ),
                )
              ],
            ),
          ),
        ),
        onPressed: (AnimationController controller) {
//          Timer(Duration(milliseconds: 800), (){
//            Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()));
//          });
          if (controller.isCompleted) {
            controller.reverse();
          } else {
            controller.forward();
          }
        },
      ),
    );
  }
}
