import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:rialotestproject/color/CustomColors.dart';
class Empty extends StatelessWidget {
  int columnCount = 3;
  @override
  Widget build(BuildContext context) {
    var imgs = [
      'assets/images/icon-mono-apple.png' ,
      'assets/images/icon-mono-banana.png' ,
      'assets/images/icon-mono-kiwi.png' ,
      'assets/images/icon-mono-avocado.png' ,
      'assets/images/icon-mono-lemon.png' ,
      'assets/images/icon-mono-orange.png' ,
      'assets/images/icon-mono-peach.png' ,
      'assets/images/icon-mono-pear.png' ,
      'assets/images/icon-mono-strawberry.png' ,
    ];
    var title = [
      'apple',
      'banana',
      'avocado',
      'kiwi',
      'lemon',
      'orange',
      'peach',
      'pear',
      'strawberry',
      '',
      '',
      '',
    ];
    return Scaffold(
      backgroundColor: CustomColors.primaryColor,
      body: Container(
        padding: EdgeInsets.only(top: 25 , bottom: 25 ,left: 25 ,right: 25),
        child:  AnimationLimiter(
          child: GridView.count(
            physics: BouncingScrollPhysics(),
            crossAxisCount: columnCount,
            childAspectRatio: 0.6,
            children: List.generate(
              9, (int index) {
              return AnimationConfiguration.staggeredGrid(
                position: index,
                duration: const Duration(milliseconds: 375),
                columnCount: columnCount,
                child: SlideAnimation(
                  child: ScaleAnimation(
                    child:Container(
                      margin: EdgeInsets.only(top: 3 , bottom: 3 ,left: 5 ,right: 5),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: MediaQuery.of(context).size.width*0.3-50,
                            width: MediaQuery.of(context).size.width*0.3-50,
                            child: Image.asset(imgs[index]),
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(top: 10),
                              child: Center(
                                child: Text(title[index] , style: TextStyle(
                                    color: CustomColors.lightText ,
                                    fontSize: 18
                                ),),
                              )
                          ) ,
                          Container(
                            margin: EdgeInsets.only(top: 15),
                            width: MediaQuery.of(context).size.width,
                            height: 45,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              color: CustomColors.darkBackColor,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text('-',
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: CustomColors.appBarText ,
                                        fontSize: 17
                                    ),),
                                ) ,
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text('0',
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: CustomColors.appBarText ,
                                        fontSize: 17
                                    ),),
                                ) ,
                                Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text('+',
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: CustomColors.appBarText ,
                                        fontSize: 17
                                    ),),
                                ) ,
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
            ),
          ),
        ),
      ),
    );
  }
}
