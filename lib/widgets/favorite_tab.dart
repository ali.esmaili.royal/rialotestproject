import 'package:flutter/material.dart';
import 'package:rialotestproject/color/CustomColors.dart';
class FavoriteTab extends StatefulWidget {
  @override
  _FavoriteTabState createState() => _FavoriteTabState();
}

class _FavoriteTabState extends State<FavoriteTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.primaryColor,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: CustomColors.darkBackColor,
        leading: Container(),
        title: Text('Favorite' , style: TextStyle(
            color: CustomColors.appBarText
        ),),
      ),
    );
  }
}
