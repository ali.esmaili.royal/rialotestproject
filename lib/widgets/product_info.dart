import 'package:flutter/material.dart';
import 'package:rialotestproject/color/CustomColors.dart';
class ProductInfo extends StatelessWidget {
  var nutritionalInformation , amount ;
  ProductInfo({this.nutritionalInformation , this.amount});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10 , bottom: 10 , left:  MediaQuery.of(context).size.width*0.1 , right:  MediaQuery.of(context).size.width*0.1),
      child:Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(nutritionalInformation, style: TextStyle(
              color: CustomColors.appBarText ,
              fontSize: 18
          ),),
          Text(amount , style: TextStyle(
              color: CustomColors.lightText ,
              fontSize: 18
          ),),
        ],
      ) ,
    );
  }
}
