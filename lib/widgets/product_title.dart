import 'package:flutter/material.dart';
import 'package:rialotestproject/color/CustomColors.dart';

class ProductTitle extends StatelessWidget {
  var txt;
  ProductTitle({this.txt});
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(top: 30 , bottom: 30 , left:  MediaQuery.of(context).size.width*0.1 , right:  MediaQuery.of(context).size.width*0.1),
        child:  Text(txt ,
          textAlign: TextAlign.left,
          style: TextStyle(
              color: CustomColors.accentColor ,
              fontSize: 23
          ),),
    ) ;
  }
}
