import 'package:flutter/material.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/product.dart';
import 'package:rialotestproject/widgets/cart_tab.dart';
import 'package:rialotestproject/widgets/emptyTab.dart';
import 'package:rialotestproject/widgets/store_tab.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
class QuickShopTab extends StatefulWidget {
  @override
  _QuickShopTabState createState() => _QuickShopTabState();
}

class _QuickShopTabState extends State<QuickShopTab> with SingleTickerProviderStateMixin{
  TabController _tabController;
  @override
  void initState() {
    _tabController =
        TabController(length: 4, vsync: this);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.primaryColor,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: CustomColors.darkBackColor,
        leading: IconButton(icon: ImageIcon(new AssetImage('assets/images/refrech.png' ) , size: 25 , color: CustomColors.lightText,)
          , onPressed: (){
          },
        ),
        actions: <Widget>[
          IconButton(icon: ImageIcon(new AssetImage('assets/images/check.png' ) , size: 25 , color: CustomColors.accentColor,)
            , onPressed: (){
            },
          )
        ],
        title: Text('Quick shop' , style: TextStyle(
            color: CustomColors.appBarText
        ),),
        bottom: TabBar(
          controller: _tabController,
          isScrollable: false,
          indicatorSize: TabBarIndicatorSize.tab,
          indicator: new BubbleTabIndicator(
           indicatorHeight: 50.0,
            indicatorColor: CustomColors.accentColor,
            tabBarIndicatorSize: TabBarIndicatorSize.tab,
            insets: EdgeInsets.only(bottom: 3 , left: 8 , right: 8)
          ),
          tabs: [
            Tab(text: "All" ),
            Tab(text: "Fruit"),
            Tab(text: "Veg"),
            Tab(text: "Nuts"),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          Empty() ,
          Empty() ,
          Empty() ,
          Empty() ,
        ],
      ),
    );
  }
}
