import 'package:flutter/material.dart';
import 'package:rialotestproject/color/CustomColors.dart';
class SettingsTab extends StatefulWidget {
  @override
  _SettingsTabState createState() => _SettingsTabState();
}

class _SettingsTabState extends State<SettingsTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.primaryColor,
      appBar:AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: CustomColors.darkBackColor,
        leading: Container(),
        title: Text('Settings' , style: TextStyle(
            color: CustomColors.appBarText
        ),),
      ),
    );
  }
}
