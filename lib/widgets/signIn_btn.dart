import 'dart:async';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:provider/provider.dart';
import 'package:rialotestproject/Service/authService.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/main_page.dart';
import 'dart:io' show Platform;

import 'package:rialotestproject/models/authField.dart';
class SignInBtn extends StatefulWidget {
  AuthField  authFiled ;
  SignInBtn(this.authFiled);
  @override
  _SignInBtnState createState() => _SignInBtnState(this.authFiled);
}

class _SignInBtnState extends State<SignInBtn> {
  AuthField  authFiled ;
  _SignInBtnState(this.authFiled);
  @override
  Widget build(BuildContext context) {

    return  Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.1 ),
      width: MediaQuery.of(context).size.width ,
      height: 55,
      padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
      child: ProgressButton(
        color: CustomColors.accentColor,
        borderRadius: BorderRadius.all(Radius.circular(100)),
        strokeWidth: 2,
        child:Container(
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/images/right.png' , width: 20 , height: 25,) ,
                SizedBox(
                  width: 5,
                ),
                Text(
                  "SIGN IN",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      height: 1.5
                  ),
                )
              ],
            ),
          ),
        ),
        onPressed: (AnimationController controller) async{
//          Timer(Duration(milliseconds: 900), (){
//            if(Platform.isAndroid)
//            {
//              Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, curve: Curves.easeOut, child: MainPage()));
//            }else
//            {
//              Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()));
//            }
//          });
          AuthService authService = AuthService() ;

          print(authFiled.getMail() +' ' + authFiled.getPass()) ;
          //await authService.signIn(controller, this.email, this.pass);

//          Timer(Duration(milliseconds: 500), (){
//            controller.reverse();
//          });
//          if (controller.isCompleted) {
//            controller.reverse();
//          } else {
//            controller.forward();
//          }
        },
      ),
    ) ;
  }
}