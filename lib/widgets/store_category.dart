import 'dart:async';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:rialotestproject/Service/vegetables.dart';
import 'package:rialotestproject/bloc/cart_count.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:rialotestproject/models/vegetables.dart';
import 'dart:io' show Platform;

import '../product.dart';
class StoreCategory extends StatefulWidget {
    final CounterBloc counterBloc ; 
   StoreCategory({this.counterBloc}) ;
  @override
  _StoreCategoryState createState() => _StoreCategoryState();
}

class _StoreCategoryState extends State<StoreCategory> with TickerProviderStateMixin {


  Future<Vegetables> vegetables ;

  var img = ['assets/images/strawberry.png' ,
    'assets/images/orange.png' ,
    'assets/images/banana.png' ,
    'assets/images/strawberry.png' ,
    'assets/images/strawberry.png' ,
    'assets/images/strawberry.png'
  ];
  var catVisibility = false ;
  var service = VegetablesService() ;
  @override
  void initState() {

    vegetables = service.getVegetables();

    print(vegetables) ;
    Timer(Duration(milliseconds: 300), (){
      setState(() {
        catVisibility = true ;
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var mediaQuery  =  MediaQuery.of(context).size;
    return Container(
      height: 220,
      width: mediaQuery.width,
      child: Visibility(
        visible: catVisibility,
        child: FutureBuilder<Vegetables>(
          future: vegetables,
          builder: (context ,AsyncSnapshot snapshot){
            return snapshot.connectionState == ConnectionState.done ? snapshot.hasData ?
              AnimationLimiter(
                  child: ListView.builder(
                    itemCount: snapshot.data.data.length,
                    scrollDirection: Axis.horizontal,
                    physics: BouncingScrollPhysics(),
                    itemBuilder: (context , index){
                      return AnimationConfiguration.staggeredList(
                        position: index,
                        child: FlipAnimation(
                          child: ScaleAnimation(
                            child: Stack(
                              children: <Widget>[
                                Container(
                                    width: 150,
                                    margin: EdgeInsets.only(left: 7 , right: 7 , top: 10 , bottom: 10),
                                    decoration: BoxDecoration(
                                      color: index==0?CustomColors.redColor :
                                      index==1?CustomColors.orangeColor :
                                      index==2?CustomColors.yellowColor : CustomColors.lightGreenColor,
                                      borderRadius: BorderRadius.all(Radius.circular(15)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: 2.0, // soften the shadow
                                          spreadRadius: 2.0, //extend the shadow
                                          offset: Offset(
                                            1, // Move to right 10  horizontally
                                            2, // Move to bottom 10 Vertically
                                          ),
                                        )
                                      ],
                                    ),
                                    child: InkWell(
                                      onTap: (){
                                        if(Platform.isAndroid)
                                        {
                                          Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, curve: Curves.easeOut, child: Product(
                                            img:snapshot.data.data[index].image ,
                                            title: snapshot.data.data[index].name,
                                            description: snapshot.data.data[index].description,
                                            counterBloc: widget.counterBloc,
                                          )));
                                        }else
                                        {
                                          Navigator.push(context, MaterialPageRoute(builder: (context)=>Product(
                                            img:snapshot.data.data[index].image ,
                                            title: snapshot.data.data[index].name,
                                            description: snapshot.data.data[index].description,
                                            counterBloc: widget.counterBloc,
                                          )));
                                        }
                                      },
                                      child: Column(
                                        children: <Widget>[
                                          SizedBox(
                                            height: 30,
                                          ),
                                          Container(
                                            width: 120 ,  height: 120,
                                            child: Center(
                                              child: CachedNetworkImage(
                                                imageUrl: snapshot.data.data[index].image,
                                                progressIndicatorBuilder: (context, url, downloadProgress) =>
                                                    CupertinoActivityIndicator(),
                                                //  errorWidget: (context, url, error) => Icon(Icons.error),
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(snapshot.data.data[index].name ,
                                            textAlign: TextAlign.right,
                                            style: TextStyle(
                                              fontSize: 18 ,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white ,
                                            ),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    )
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                )
                  :
              Container(
                child: Center(
                  child:  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 30,
                      ),
                      Text('Something went wrong!' , style: TextStyle(
                          color: CustomColors.appBarText
                      ),) ,
                      SizedBox(
                        height: 10,
                      ),
                      FlatButton(onPressed: (){
                        setState(() {
                          snapshot = null ;
                          vegetables = service.getVegetables();
                        });
                      }, child: Text('Try again' , style: TextStyle(
                          color: CustomColors.lightText
                      ),)),
                    ],
                  ),
                ),
              )
                  :
              Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
          },
        ),
      ),
    );
  }
}
