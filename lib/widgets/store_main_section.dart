import 'package:flutter/material.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/test.dart';

class StoreMainSection extends StatefulWidget {
  @override
  _StoreMainSectionState createState() => _StoreMainSectionState();
}

class _StoreMainSectionState extends State<StoreMainSection> {

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(bottomRight: Radius.circular(30) , bottomLeft: Radius.circular(30)),
        color: CustomColors.darkBackColor,
      ),
      margin: EdgeInsets.only(bottom: 20),
      child: Column(
        children: <Widget>[
          GestureDetector(
            child:  Container(
                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.1 , right: MediaQuery.of(context).size.width*0.1),
                width: MediaQuery.of(context).size.width*0.65,
                child:Container(
                  child:Image.asset('assets/images/broccoli.png',
                    fit: BoxFit.contain,
                  ),
                )
            ) ,
            onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Test()));

//              if(Platform.isAndroid)
//              {
//                Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, curve: Curves.easeOut, child: Product()));
//              }else
//              {
//                Navigator.push(context, MaterialPageRoute(builder: (context)=>Product()));
//              }
            },
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 35),
              child: Center(
                child: Text('Vegetables ' , style: TextStyle(
                    color: CustomColors.appBarText ,
                    fontSize: 30
                ),),
              )
          ) ,
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 10),
              child: Center(
                child: Text('Browse ' , style: TextStyle(
                    color: CustomColors.lightText ,
                    fontSize: 18
                ),),
              )
          ) ,
          SizedBox(
            height:15,
          )
        ],
      ),
    );
  }
}
