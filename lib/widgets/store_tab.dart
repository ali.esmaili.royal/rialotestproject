import 'package:flutter/material.dart';
import 'package:rialotestproject/bloc/cart_count.dart';
import 'package:rialotestproject/color/CustomColors.dart';
import 'package:rialotestproject/widgets/store_category.dart';
import 'package:rialotestproject/widgets/store_main_section.dart';
class StoreTab extends StatefulWidget {
StoreTab({Key key,this.counterBloc}) : super(key: key);

final CounterBloc counterBloc ;
  @override
  _StoreTabState createState() => _StoreTabState();
}

class _StoreTabState extends State<StoreTab> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.primaryColor,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: CustomColors.darkBackColor,
        leading: Container(),
        actions: <Widget>[
          IconButton(icon: ImageIcon(new AssetImage('assets/images/search.png') , color: CustomColors.appBarText,), onPressed: (){
            print('test');
          })
        ],
          title: Text('Store' , style: TextStyle(
            color: CustomColors.appBarText
          ),),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            StoreMainSection() ,
            StoreCategory(counterBloc : widget.counterBloc)
          ],
        ),
      ),
    );
  }
}
